import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FrameOne extends JFrame {
	private static final long serialVersionUID = 1L;
	int timesClosed = 0;

	public FrameOne() {
		this.setSize(400, 600);
		JButton btn = new JButton("Open Frame Two");
		this.add(btn);

		// Just a quick reference of this object instance so that we can pass it along
		// into the anonymous ActionListener implementation below.
		FrameOne ref = this;

		// click action
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Here I create a new FrameTwo, pass along the reference to this frame (its
				// parent) and I call setVisible after it's been created.
				new FrameTwo(ref).setVisible(true);
			}
		});

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void FrameTwoWasClosed() {
		timesClosed++;
		System.out.println("Frame two was closed " + timesClosed + " times");
	}

}
